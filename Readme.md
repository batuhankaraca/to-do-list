To-Do-List Application

Merhaba. Ben Batuhan Karaca.
Uygulaman�n front-end taraf� Angular ile geli�tirilmi� olup, back-end taraf� ise PHP ile geli�tirilmi�tir. Uygulamay� �al��t�rman�z i�in gereken i�lemleri s�yleyece�im. Ad�m ad�m uyguland���nda ba�ar�yla �al��acakt�r.


1. git clone https://batuhankaraca@bitbucket.org/batuhankaraca/to-do-list.git  yaparak dosyay� kendi bilgisayar�m�za �ekiyoruz.
2. �ndirilen dosyan�n i�erisinde terminal yard�m�yla cd .\to-do-list\ yaparak giriyoruz.
3. Girilen dosyan�n i�erisinde npm install diyerek gerekli olan node-module�leri y�kl�yoruz.
4. Geriye uygulamam�z� ba�latmak kal�yor. Bunun i�in ise ng serve --open komutunu terminal �zerinden yaz�yoruz.
5. Komut y�kleme yapt�ktan sonra http://localhost:4200 �zerinden a��lacakt�r.
6. Uygulaman�n back-end taraf�n� kurulum ile u�ra��lmamas� ad�na kendi domain adresim olan https://www.batukaraca.com/huawei/server/ i�erisine ta��d�m. Bu sebeple server�n �al��mas� i�i herhangi bir ayar yap�lmas�na gerek yoktur.
7. �ndirilen dosyan�n i�erisinde server dosyas�nda back-end taraf�nda neler yap�ld���n� g�rebilirsiniz. Server dosyas�n�n i�erisinde ayr�ca sql isimli dosya bulunmaktad�r. Bu dosyada veritaban�nda hangi tablolar�n bulundu�u g�z�kmektedir. Gerekli g�r�l�rse local tarafta nas�l �al��t�r�laca��n� da a��klayabilirim.



Unit Tests
1. to-do-list dosyas� i�erisinde ng test yazarak olu�turdu�um test sonu�lar�na ula�abilirsiniz. Authentication klas�r�nde olu�turdu�um testte bir hata vermesi sebepiyle yorum sat�r�na ald�m. Yorum sat�r�n� kald�r�p, verilen hatay� g�rebilirsiniz. Sebebini �u an i�in bulamad�m, ara�t�r�yorum.
