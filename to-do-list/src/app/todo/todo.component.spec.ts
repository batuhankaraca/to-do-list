import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TodoComponent } from './todo.component';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';

describe('TodoComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        AppRoutingModule,
      ],
      declarations: [
        TodoComponent,
      ],
      providers: [HttpClientModule, { provide: APP_BASE_HREF, useValue: '/' }],
    }).compileComponents();
  }));
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TodoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.getLists).toBeTruthy();
    expect(component.getTodos).toBeTruthy();
  });

  it('should called for lists', async(() => {
    spyOn(component, 'ListProcess');

    const button = fixture.debugElement.nativeElement.querySelector('.createbtn');
    button.click();

    fixture.whenStable().then(() => {
      expect(component.ListProcess).toHaveBeenCalled();
    });
  }));

  it('should called for items', async(() => {
    spyOn(component, 'ToDoProcess');

    const button = fixture.debugElement.nativeElement.querySelector('#itembtn');
    button.click();

    fixture.whenStable().then(() => {
      expect(component.ToDoProcess).toHaveBeenCalled();
    });
  }));
});
