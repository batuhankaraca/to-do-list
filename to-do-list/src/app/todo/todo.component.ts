import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Items } from '../modals/items';
import { Lists } from '../modals/lists';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  public item: Items = new Items();
  public list: Lists = new Lists();
  public liste: Lists = new Lists();
  public items: Items[] = [];
  public allitems: Items[] = [];
  public lists: Lists[] = [];
  listename: string;
  selectedListid: number;
  isValid = false;
  missInfo = false;
  noItem = false;
  noList = false;
  searchText = '';
  selectedStatus = 0;
  sortItems: string;


  constructor(public api: ApiService, private spinner: NgxSpinnerService, private route: Router) { }

  ngOnInit() {
    this.list.id = 1;
    this.item.list_id = 2;
    this.item.dependency_id = 0;
    this.isValid = false;
    this.getLists();
    this.getTodos();
    this.sortItems = '0';
  }

  DependencyOption(id: any) {
    this.item.dependency_id = id;
  }

  ListProcess(value: number, list: Lists) {
    this.list.user_id = this.api.user.id;
    this.list.operation_type = value;
    switch (value) {
      case 1: /*new*/
        if (this.list.name && this.list.name !== '') {
          this.isValid = true;
          this.missInfo = false;
        } else {
          this.isValid = false;
          this.missInfo = true;
        }
        break;
      case 2: /*delete*/
        this.list.id = list.id;
        this.isValid = true;
        break;
    }
    if (this.isValid) {
      this.spinner.show();
      this.api.ListOperation(this.list).then((res: any) => {
        if (res.success) {
          this.spinner.hide();
          this.list.name = '';
          this.getLists();
        } else {
          this.spinner.hide();
        }
      });
    }
  }

  getLists() {
    this.spinner.show();
    this.api.getLists(this.api.user?.id).then((res: any) => {
      if (res.success) {
        this.spinner.hide();
        this.lists = res.lists;
        this.selectedListid = this.lists[0].id;
        this.noList = false;
        this.noItem = false;
      } else {
        this.spinner.hide();
        this.noList = true;
        this.noItem = true;
      }
    });
  }

  selectedList(id: number) {
    this.selectedListid = id;
  }

  ItemsOfLists(id: any) {
    this.items = this.allitems;

    if (this.sortItems) {
      switch (this.sortItems) {
        case '0': /*Deadline*/
          this.items = this.items.sort((a, b) => new Date(a.deadline).getTime() - new Date(b.deadline).getTime());
          break;
        case '1': /*Create Date*/
          this.items = this.items.sort((a, b) => new Date(a.create_date).getTime() - new Date(b.create_date).getTime());
          break;
        case '2': /*Name*/
          this.items = this.items.sort((a, b) => a.name.localeCompare(b.name));
          break;
        case '3': /*Status*/
          this.items = this.items.sort((a, b) => a.status - b.status);
          break;
      }
    }

    if (this.searchText !== '' && this.selectedStatus < 1) { /*Name and Desc Filter*/
      this.items = this.items.filter(x => x.list_id === id);
      return this.items.filter((item) =>
        item.name.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1 ||
        item.description.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1);
    }
    else if (this.selectedStatus > 0 && this.searchText === '') { /*Status Filter*/
      this.items = this.items.filter(x => x.list_id === id);
      return this.items.filter(x => x.status === this.selectedStatus);
    }
    else if (this.selectedStatus > 0 && this.searchText !== '') { /*Status and Name - Desc Filter*/
      this.items = this.items.filter(x => x.list_id === id);
      return this.items.filter((item) =>
        (item.name.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1 ||
          item.description.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1) &&
        item.status === this.selectedStatus);
    }
    else { /*List Filter*/
      return this.items.filter(x => x.list_id === id);
    }
  }


  ToDoProcess(value: number, item: Items) {
    this.item.user_id = this.api.user.id;
    this.item.operation_type = value;
    this.item.list_id = this.selectedListid;

    switch (value) {
      case 1: /*new item*/
        // tslint:disable-next-line:max-line-length
        if (this.item.name && this.item.description && this.item.deadline && this.item.name !== '' && this.item.description !== '' && this.item.deadline !== null) {
          this.isValid = true;
          this.missInfo = false;
        } else {
          this.isValid = false;
          this.missInfo = true;
        }
        break;
      case 2: /*delete item*/
        this.item.id = item.id;
        this.isValid = true;
        break;
      case 3: /*update item*/
        this.item.id = item.id;
        if (item.dependency_id > 0) {
          const depenitem = this.items.find(x => x.id === item.dependency_id);

          if (depenitem.status > 1) {
            this.isValid = true;
          }
          else {
            alert('Complete ' + depenitem.name + ' First');
            this.isValid = false;
          }
        }
        else {
          this.isValid = true;
        }
        break;
    }

    if (this.isValid) {
      this.spinner.show();
      this.api.ToDoOperation(this.item).then((res: any) => {
        if (res.success) {
          this.spinner.hide();
          this.getTodos();
        } else {
          this.spinner.hide();
        }
      });
    }

  }

  getTodos() {
    this.spinner.show();
    this.api.getItems(this.api.user?.id).then((res: any) => {
      if (res.success) {
        this.spinner.hide();
        this.items = res.items.sort((a, b) => new Date(a.deadline).getTime() - new Date(b.deadline).getTime());
        this.allitems = this.items;
        this.selectedListid = this.items[0].list_id;
        this.noItem = false;
      } else {
        this.spinner.hide();
        this.noItem = true;
      }
    });
  }
}
