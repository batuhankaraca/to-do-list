import { Component } from '@angular/core';
import { ApiService } from './api/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'to-do-list';

  constructor(public api: ApiService, private route: Router) {
    this.api.user = JSON.parse(localStorage.getItem('user'));
  }

  Logout() {
    localStorage.removeItem('user');
    this.route.navigateByUrl('/');
    window.location.reload();
  }
}
