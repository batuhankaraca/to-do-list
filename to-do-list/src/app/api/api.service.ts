import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { User } from '../modals/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public user: User;
  loginmenu = false;

  // tslint:disable-next-line:variable-name -- LOCAL SERVER
  /*private base_url: any = 'http://localhost/works/Huawei/server/';*/

  /*My Domain*/
  // tslint:disable-next-line:variable-name
  private base_url: any = 'https://www.batukaraca.com/huawei/server/';
  // tslint:disable-next-line:variable-name
  private register_user_url: string = this.base_url + 'ws/register_user.php';
  // tslint:disable-next-line:variable-name
  private check_login_url: string = this.base_url + 'ws/check_login.php';
  // tslint:disable-next-line:variable-name
  private list_operations_url: string = this.base_url + 'ws/list_operations.php';
  // tslint:disable-next-line:variable-name
  private get_lists_url: string = this.base_url + 'ws/get_lists.php';
  // tslint:disable-next-line:variable-name
  private item_operations_url: string = this.base_url + 'ws/item_operations.php';
  // tslint:disable-next-line:variable-name
  private get_items_url: string = this.base_url + 'ws/get_items.php';

  constructor(private http: HttpClient) { }

  handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


  register(account: any): Promise<string> {
    return this.http
      .post(this.register_user_url, account, { responseType: 'json' })
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(this.handleError);
  }

  checkLogin(account: any): Promise<string> {
    return this.http
      .post(this.check_login_url, account)
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(this.handleError);
  }

  getItems(account: number): Promise<string> {
    return this.http
      .post(this.get_items_url, account, { responseType: 'json' })
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(this.handleError);
  }

  ToDoOperation(items: any): Promise<string> {
    return this.http
      .post(this.item_operations_url, items, { responseType: 'json' })
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(this.handleError);
  }

  ListOperation(lists: any): Promise<string> {
    return this.http
      .post(this.list_operations_url, lists, { responseType: 'json' })
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(this.handleError);
  }

  getLists(account: number): Promise<string> {
    return this.http
      .post(this.get_lists_url, account, { responseType: 'json' })
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(this.handleError);
  }
}
