import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  missinfo = false;
  wronginfo = false;
  public account: { email: string, password: string } = {
    email: '',
    password: ''
  };

  constructor(public api: ApiService, private route: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    if (this.api.user) {
      this.route.navigateByUrl('/');
    }
    this.api.loginmenu = true;
    this.missinfo = false;
    this.wronginfo = false;
  }

  async Login() {
    if (this.account.email !== '' && this.account.password !== '') {
      this.missinfo = false;
      this.wronginfo = false;
      this.spinner.show();
      this.api.checkLogin(this.account).then((res: any) => {
        if (res.success) {
          this.spinner.hide();
          this.api.user = res.user;
          localStorage.setItem('user', JSON.stringify(this.api.user));
          console.log('user', res.user);
          this.goToHome();
        } else {
          this.spinner.hide();
          this.wronginfo = true;
        }
      });
    } else {
      this.missinfo = true;
    }
  }

  goToHome() {
    this.route.navigateByUrl('/');
  }

}
