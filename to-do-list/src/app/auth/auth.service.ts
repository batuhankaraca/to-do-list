
import { Injectable } from '@angular/core';

export const TOKEN_NAME = 'user';

@Injectable()
export class AuthService {


    constructor() { }

    getToken(): boolean {
        const user = localStorage.getItem(TOKEN_NAME);
        if (user) {
            return true;
        }
        else {
            return false;
        }
    }
}
