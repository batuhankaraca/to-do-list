import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  missinfo = false;
  wronginfo = false;

  public account: { first_name: string, last_name: string, email: string, password: string } = {
    first_name: '',
    last_name: '',
    email: '',
    password: ''
  };

  constructor(public api: ApiService, private route: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    if (this.api.user) {
      this.route.navigateByUrl('/');
    }
    this.api.loginmenu = true;
    this.missinfo = false;
    this.wronginfo = false;
  }

  async signUp() {
    if (this.account.email !== '' && this.account.first_name !== '' && this.account.last_name !== '' && this.account.password !== '') {
      this.missinfo = false;
      this.wronginfo = false;
      this.spinner.show();
      this.api.register(this.account).then((res: any) => {
        if (res.success) {
          this.spinner.hide();
          this.route.navigateByUrl('/login');

        } else {
          this.spinner.hide();
          this.wronginfo = true;
        }
      });
    } else {
      this.missinfo = true;
    }

  }

}
