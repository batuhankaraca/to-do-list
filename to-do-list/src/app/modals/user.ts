export class User {
    id: number;
    // tslint:disable-next-line:variable-name
    first_name: string;
    // tslint:disable-next-line:variable-name
    last_name: string;
    password: string;
    email: string;
    status: number;
}
