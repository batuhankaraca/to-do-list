export class Items {
    id: number;
    // tslint:disable-next-line:variable-name
    user_id: number;
    // tslint:disable-next-line:variable-name
    dependency_id: number;
    // tslint:disable-next-line:variable-name
    list_id: number;
    name: string;
    description: string;
    deadline: Date;
    status: number;
    // tslint:disable-next-line:variable-name
    create_date: Date;
    // tslint:disable-next-line:variable-name
    operation_type: number;
}
