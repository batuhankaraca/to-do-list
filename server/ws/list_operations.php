<?php

include "headers.php";
include '../config_db.php';
include "../dao/lists.php";
include "../dao/items.php";
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = '';
$form = json_decode(file_get_contents('php://input'));

if ($form) {
    $list = new Lists();

    //CREATE
    if($form->operation_type == 1){
        $list->user_id = $form->user_id;
        $list->name = $form->name;
        $result["success"] = $list->insert();
        $result["list"] = $list;
    }

    //DELETE
    if($form->operation_type == 2){
        $list->id = $form->id;
        $list->read();
        $result["success"] = $list->delete();

        $item = new Items();
        $where = 'list_id = ' . "'" . $form->id . "'";
        $item = $item->deleteMultiRow('items', $where);
        $result["list"] = $item;
    }
    $result["error"] = $list->error;
}
echo json_encode($result);
?>