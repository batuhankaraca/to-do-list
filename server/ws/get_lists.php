<?php

include "headers.php";
include '../config_db.php';
include "../dao/lists.php";
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = '';
$id = json_decode(file_get_contents('php://input'));
if ($id) {
    $list = new Lists();
    $where = 'user_id = ' . "'" . $id . "'";
    $list = $list->readAll(NULL, $where);
    
    if(!$list){
        $result["error"] = 'No List Found';
    }
    else{
        $result["lists"] = $list;
        $result["success"] = TRUE;
    }
}
else{
    $result["error"] = 'User Not Found';
}
echo json_encode($result);
?>