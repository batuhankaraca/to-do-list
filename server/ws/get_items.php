<?php

include "headers.php";
include '../config_db.php';
include "../dao/items.php";
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = '';
$id = json_decode(file_get_contents('php://input'));
if ($id) {
    $items = new Items();
    $where = 'user_id = ' . "'" . $id . "'";
    $items = $items->readAll(NULL, $where);

    $now = new DateTime();
    $now->format('Y-m-d H:i:s');
    
    foreach ($items as $item) {
        $deadline = new DateTime($item->deadline);
        $deadline->format('Y-m-d H:i:s');

        if( $deadline->getTimestamp() < $now->getTimestamp()){
            $item->read();
            $item->status = '3';
            $item->update();
        }
    }

    if(!$items){
        $result["error"] = 'No Item Found';
    }
    else{
        $result["items"] = $items;
        $result["success"] = TRUE;
    }
}
else{
    $result["error"] = 'User Not Found';
}
echo json_encode($result);
?>