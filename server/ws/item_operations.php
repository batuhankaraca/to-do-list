<?php

include "headers.php";
include '../config_db.php';
include "../dao/items.php";
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = '';
$form = json_decode(file_get_contents('php://input'));

if ($form) {
    $item = new Items();

    //CREATE
    if($form->operation_type == 1){
        $item->user_id = $form->user_id;
        $item->name = $form->name;
        $item->description = $form->description;
        $item->deadline = $form->deadline;
        $item->dependency_id = $form->dependency_id;
        $item->list_id = $form->list_id;
        $item->status = 1;
        $item->create_date = date("d-m-Y");
        $result["success"] = $item->insert();
        $result["item"] = $item;
    }

    //DELETE
    if($form->operation_type == 2){
        $item->id = $form->id;
        $item->read();
        $result["success"] = $item->delete();
        $result["item"] = 'DELETED';
    }

    //UPDATE
    if($form->operation_type == 3){
        $item->id = $form->id;
        $item->read();
        $item->status = 2;
        $result["success"] = $item->update();
        $result["item"] = 'UPDATED';
    }
    $result["error"] = $item->error;
}
echo json_encode($result);
?>