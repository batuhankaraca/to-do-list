<?php

include "headers.php";
include '../config_db.php';
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = '';
$form = json_decode(file_get_contents('php://input'));

if ($form) {
    $user = new User();

    $user->first_name = $form->first_name;
    $user->last_name = $form->last_name;
    $user->email = $form->email;
    $user->password = md5($form->password);
    $result["success"] = $user->insert();
    $result["error"] = $user->error;
    $result["user"] = $user;
}
echo json_encode($result);
?>