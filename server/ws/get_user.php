<?php

include "headers.php";
include "../config_db.php";
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = '';

$id = json_decode(file_get_contents('php://input'));
if ($id) {
    $item = new User();
    $whereClause = 'id = ' . $id;
    $item->readWithWhere($whereClause);
    if ($item != NULL) {
        $result['success'] = TRUE;
        $result['user'] = $item;
    } else {
        $result["success"] = FALSE;
    }
    $result['error'] = $item->error;    
}
echo json_encode($result);
?>
