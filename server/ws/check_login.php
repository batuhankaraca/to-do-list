<?php
include "headers.php";
include "../config_db.php";
include "../dao/user.php";

$result = array();
$result["success"] = FALSE;
$result["error"] = 'Unknown Error';

$account = json_decode(file_get_contents('php://input'));
if ($account) {
    $email = $account->email;
    $password = $account->password;

    $item = new User();
    $item = User::checkLogin($email, $password);
    $result = array();

    if ($item != NULL) {
        $result["success"] = TRUE;
        $result["user"] = $item;
    } else {
        $result["success"] = FALSE;
        $result["error"] = "Invalid username or password.";
    }    
}

echo json_encode($result);
?>
