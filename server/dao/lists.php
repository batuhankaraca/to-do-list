<?php


require_once ( "../lib/database/dao.php");
require_once ( "../lib/database/query_helper.php");
require_once ( "../lib/database/data_type.php");

class Lists extends DAO {

    const table_name = "lists";
    const col_id = "id";
    const col_user_id = "user_id";
    const col_name = "name";

    var $id, $user_id, $name;

    protected function init() {
        $this->setTableName(self::table_name);

        $this->addColumn("id", self::col_id, DataType::Integer, TRUE, TRUE);
        $this->addColumn("user_id", self::col_user_id, DataType::Integer, FALSE, FALSE);
        $this->addColumn("name", self::col_name, DataType::String, FALSE, FALSE);       
    }
}

?>
