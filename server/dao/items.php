<?php


require_once ( "../lib/database/dao.php");
require_once ( "../lib/database/query_helper.php");
require_once ( "../lib/database/data_type.php");

class Items extends DAO {

    const table_name = "items";
    const col_id = "id";
    const col_user_id = "user_id";
    const col_list_id = "list_id";
    const col_dependency_id = "dependency_id";
    const col_name = "name";
    const col_description = "description";
    const col_deadline = "deadline";
    const col_status = "status";
    const col_create_date = "create_date";

    var $id, $user_id, $list_id, $dependency_id, $name, $description, $deadline,  $status, $create_date;

    protected function init() {
        $this->setTableName(self::table_name);

        $this->addColumn("id", self::col_id, DataType::Integer, TRUE, TRUE);
        $this->addColumn("user_id", self::col_user_id, DataType::Integer, FALSE, FALSE);
        $this->addColumn("list_id", self::col_list_id, DataType::Integer, FALSE, FALSE);
        $this->addColumn("dependency_id", self::col_dependency_id, DataType::Integer, FALSE, FALSE);
        $this->addColumn("name", self::col_name, DataType::String, FALSE, FALSE);       
        $this->addColumn("description", self::col_description, DataType::String, FALSE, FALSE);
        $this->addColumn("deadline", self::col_deadline, DataType::Date, FALSE, FALSE);
        $this->addColumn("status", self::col_status, DataType::Integer, FALSE, FALSE);
        $this->addColumn("create_date", self::col_create_date, DataType::Date, FALSE, FALSE);
    }
}

?>
