<?php


require_once ( "../lib/database/dao.php");
require_once ( "../lib/database/query_helper.php");
require_once ( "../lib/database/data_type.php");

class User extends DAO {

    const table_name = "users";
    const col_id = "id";
    const col_first_name = "first_name";
    const col_last_name = "last_name";
    const col_password = "password";
    const col_email = "email";

    var $id, $first_name, $last_name,  $password,  $email;

    protected function init() {
        $this->setTableName(self::table_name);

        $this->addColumn("id", self::col_id, DataType::Integer, TRUE, TRUE);
        $this->addColumn("first_name", self::col_first_name, DataType::String, FALSE, FALSE);       
        $this->addColumn("last_name", self::col_last_name, DataType::String, FALSE, FALSE);
        $this->addColumn("password", self::col_password, DataType::String, FALSE, FALSE);
        $this->addColumn("email", self::col_email, DataType::String, FALSE, FALSE);
    }

    static function checkLogin($email, $password) {
        $where = User::col_email . " = '" . $email . "' AND " . User::col_password . " = '" . md5($password) . "' ";
        $user = new User();
        $result = $user->readAll(null, $where);
        if (count($result) != 0) {
            return $result[0];
        }
        return NULL;
    }

}

?>
