-- -----------------------------------------------------
-- Table `users`: 
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL, 
  `last_name` varchar(45) NOT NULL, 
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
   PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- -----------------------------------------------------
-- Table `lists`: 
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL, 
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- -----------------------------------------------------
-- Table `items`: 
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `dependency_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL, 
  `description` varchar(200) NOT NULL, 
  `deadline`  datetime DEFAULT NULL,
  `status` int(11) NULL COMMENT '1: Continues, 2: Completed, 3:Expired',
  `create_date`  datetime DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;